** HTTP Tunnels 
On the terminal: 
$ ssh [proxy server] -D [port] 

In the browser: 
Tell the browser's proxy configuration use SOCKS Host 127.0.0.1 and 
Port that matches your ssh command. 
