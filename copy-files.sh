#! /bin/bash

rm ../.gitconfig
rm ../.spacemacs
rm ../.zshrc
rm ../.vimrc

ln -s  /Users/samuel.olmsted/home-directory-settings/gitconfig /Users/samuel.olmsted/\.gitconfig
ln -s  /Users/samuel.olmsted/home-directory-settings/spacemacs /Users/samuel.olmsted/\.spacemacs
ln -s  /Users/samuel.olmsted/home-directory-settings/zshrc /Users/samuel.olmsted/\.zshrc
ln -s  /Users/samuel.olmsted/home-directory-settings/vimrc /Users/samuel.olmsted/\.vimrc

