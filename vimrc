"back ups drive me nuts
set nobackup

"4-space indents for the win
set expandtab
set tabstop=2

"Blink to the last matching paren, bracket, or curly
set showmatch


"no text wrapping
set nowrap

"Turn syntax off 
syntax on 

"set the background and highlighting to something nicer
"find more in /usr/share/vim/vim72/colors
colorscheme desert 

"Auto-indent for c, so I don't have to keep adjusting the spaces
set autoindent

"avoid using the escape key
:imap jj <Esc>

"set turn syntax folding on 
"set fdm=syntax
