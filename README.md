# Mac Settings 
These are the files that I use most often on a mac. The scripts and other text
files are meant to help me get the configuration I find comfortable. 


## Brew 
brew is a package manager. Normally it is installed using root. However it can
still be installed locally, with no permessions. This has two benifits. 


The first, I can delete all the files. Boom! Then everything that was 
installed is now gone. A useful bit of knowledage for people that may 
want to the security in unwinding mistakes or going back to a system like the
one when they first started playing around with brew. 

The second, you are not root. Any changes made on an account only change that
account. It also means that if your user is not root he/she can still get
packages (as long as they don't need root). The drawback is of course you can't
install items that need system control (looking at you things that require root
*cough* java 8 and LaTex) 



## Zsh
A newer shell to bash. I find that when configured with oh-my-zsh it is a better
shell. If I didn't have the configuration of oh-my-zsh I would just stick to
bash.


Why uses a shell other than bash? Using oh-my-zsh makes the terminal very
intuitive. Command line completion and information of git repos shows up like
you would expect.

Link to oh-my-zsh
https://github.com/robbyrussell/oh-my-zsh

Basic Install
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"


## Emacs 
A popular editor. Once again, the I rely on the configuration of others :-) The
configuration I use is spacemacs.


### Spacemacs 
https://github.com/syl20bnr/spacemacs

First install Emacs
$ brew tap d12frosted/emacs-plus
$ brew install emacs-plus --with-cocoa --with-gnutls --with-librsvg --with-imagemagick --with-spacemacs-icon
$ brew linkapps

Spacemacs
git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d

## Setup 
When setting up a system. The fastest way is to go: 
Brew (check out scripts foulder)
Zsh
Emacs


## Scripts 
Running the scripts will install and setup things up. However, the 
scripts may still expect that some directories exists. 

TODO add info about what directories to create. 

## Text Files 
Anything that is not a script is most likely a dot file. 
